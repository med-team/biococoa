BioCocoa
========

BioCocoa is an open source Cocoa/OpenStep framework for bioinformatics
written in Objective-C.

Project homepage: http://www.bioinformatics.org/BioCocoa


Installation on Mac OSX
=======================

Copy BioCocoa.framework to /Library/Frameworks


Installation on GNU/Linux with GNUstep
======================================

Make sure the GNUstep environment is setup properly.

     make
     make install

If you are running a GNU/Linux system that provides a package
repository (e.g. Debian) then search for BioCocoa among the packages.
We are working to make such packages available.


Release history
===============

BioCocoa 2.2.2

- Removed 64-bit hacks for older GNUstep.

BioCocoa 2.2.1

- Update license for suffix array code to Lucent Public License
  Version 1.02, look at license.txt file.
- Minor bug fixes.

BioCocoa 2.2.0

- Change license to BSD style license, look at license.txt file.
- New BCDataMatrix class for managing 2-dimensional data matrices.
- New BCGeneExpression classes for managing gene expression data including
  parsing SOFT format.
- New BCPreference class for global settings, currently being used for
  a shared data directory to hold genome sequences, gene expression
  data, etc.


BioCocoa 2.1.1

- Bug fix release for GNUstep, set proper version for framework.


BioCocoa 2.1.0 -- (found in Tags / 2.1.0)

- New BCSuffixArray and BCMCP classes for constructing disk-based
  suffix arrays of sequences and finding the maximum common prefixes
  of sequences using those suffix arrays.  This is work based upon the
  following publication:

	Scott Christley, Neil F. Lobo and Greg Madey.
	Multiple Organism Algorithm for Finding Ultraconserved Elements.
	BMC Bioinformatics, 9: 15, 2008.

- New BCCachedSequenceFile and BCCachedFastaFile classes that allow
  for reading sequence data without reading the whole sequence into
  memory.  This is useful for working with very large sequences like
  whole genomes.  A future release will support a BCCachedSequence
  which provides a consistent sequence interface regardless of whether
  the sequence is in memory or cached on disk.
- File format can be explictly provided to BCSequenceReader using BCFileFormat.
- Documentation updates.
- More complete port to GNUstep, all BCFoundation functionality should
  be available.
- Quad-fat binary supporting 32/64-bit and ppc/intel processors.


BioCocoa 2.0 -- (found in Tags / 2.0)

The first release of the new BioCocoa project

- New BCSequence and BCSymbol based data structure
- BCSequenceReader is supported for reading of sequence files (writing
  files will be implemented in later release).
- Two new example projects: Translation and Peptides
  (SequenceConverter will be back in later release).
- Headerdoc based source code documentation
- Switched to svn for versioncontrol
- Swicthed to Creative Commons license


BioCocoa 1.7 -- (found in Tags / 1.7)

This new update of the classic BioCocoa code contains the following
new features:

- BCReader, the standalone BCReader classes for sequence file IO
  (original version Peter Schols)
- EntrezController, a controller plus view for browsing and fetching
  NCBI's Entrez Database
- BCSequenceView+, a custom NSTextView for displaying biological
  sequences (original version Koen van der Drift)



BioCocoa 1.6 -- (found in Tags / 1.6)

This folder contains:

- the ProjectBuilder project of the SequenceConverter utility
  application (BioCocoa util.pbproj), which can be opened in
  ProjectBuilder on Mac OS X or in the GNUstep ProjectCenter on
  Linux/Windows (see www.gnustep.org)
- the compiled utility application SequenceConverter, ready to run on
  Mac OS X 10.2 or higher
- the ProjectBuilder project of the BioCocoa framework (BioCocoa
  Framework), which can be opened in ProjectBuilder on Mac OS X or in
  the GNUstep ProjectCenter on Linux/Windows (see www.gnustep.org)
- the compiled BioCocoa framework (BioCocoa.framework), which can be
  opened in ProjectBuilder on Mac OS X or in the GNUstep ProjectCenter
  on Linux/Windows (see www.gnustep.org)
- the GNU GPL

If you are not a developer and you are only interested in the
SequenceConverter utility application (to convert between sequence
file formats), you can remove everything in this package except for
the SequenceConverter application itself.


Changes since version 1.2:

- BC now reads SwissProt, NCBI, and PDB files
- To be consistent when reading all filetypes, 'taxon' and 'taxa' were
  changed to 'item' and 'items' respectively in BCReader. BCCreator
  still uses 'taxon' and 'taxa'.
- Added a 'fileType' key-value

Changes since version 1.1:

- BC now reads and writes the GCG-MSF format
- Added Unix and Windows line break methods for cross-platform
  compatibility
- BC now recognizes the Trees Nexus block and stores all Newick
  strings in the root dictionary
- BC returns all Nexus blocks in the root dictionary
- BC now stores the line break of the original source file in the root
  dictionary

For more information on how to use BioCocoa in your own app, see the
API docs at: http://www.bioinformatics.org/BioCocoa/docs and the
header files in the framework.


Project homepage: http://www.bioinformatics.org/BioCocoa

