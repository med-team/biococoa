//
//  BCSample.m
//  BioCocoa
//
//  Created by Scott Christley on 10/07/08.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCSample.h"

// Global list of all samples
static NSMutableDictionary *samples = nil;
static NSMutableDictionary *get_all_samples()
{
  if (!samples) {
    samples = [NSMutableDictionary new];
  }
  return samples;
}

@implementation BCSample

// Access global list of samples
+ (NSDictionary *)getAllSamples
{
  return [NSDictionary dictionaryWithDictionary: get_all_samples()];
}

+ (BCSample *)sampleWithId: (NSString *)anId
{
  NSDictionary *d = get_all_samples();
  return [d objectForKey: anId];
}

+ (void)addSample: (BCSample *)aSample
{
  NSString *id = [aSample getId];
  NSMutableDictionary *d = get_all_samples();
  [d setObject: aSample forKey: id];
}

// Default constructor
- initWithId: (NSString *)anId
{
  self = [super init];
  if (self) {
    attributes = [NSMutableDictionary new];
    [attributes setObject: anId forKey: @"id"];
  }
  return self;
}

// Accessor methods
- (NSString *)getId
{
  return [attributes objectForKey: @"id"];
}

- (id)valueForKey: (NSString *)aKey
{
  id aValue = [attributes objectForKey: aKey];
  if ((aValue) && ([aValue isKindOfClass: [NSArray class]])) {
    if ([aValue count] == 0) return nil;
    else return [aValue objectAtIndex: 0];
  }
  return aValue;
}

- (NSArray *)valuesForKey: (NSString *)aKey
{
  id aValue = [attributes objectForKey: aKey];
  if (!aValue) return [NSArray array];
  if ([aValue isKindOfClass: [NSArray class]]) {
    return aValue;
  } else
    return [NSArray arrayWithObject: aValue];
}

- (void)setValue:(id)aValue forKey:(NSString *)aKey
{
  id anEntry = [attributes objectForKey: aKey];
  if (!anEntry)
    [attributes setObject: aValue forKey: aKey];
  else {
    if ([anEntry isKindOfClass: [NSArray class]]) {
      [anEntry addObject: aValue];
    } else {
      NSMutableArray *a = [NSMutableArray arrayWithObject: anEntry];
      [a addObject: aValue];
      [attributes setObject: a forKey: aKey];
    }
  }
}

@end
