//
//  BCToolHydropathyCalculator.m
//  BioCocoa
//
//  Created by Koen van der Drift on 4/30/2005.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCToolHydropathyCalculator.h"
#import "BCSequence.h"
#import "BCAminoAcid.h"


@implementation BCToolHydropathyCalculator

+ (BCToolHydropathyCalculator *) hydropathyCalculatorWithSequence: (BCSequence *) list
{
	BCToolHydropathyCalculator *hydropathyCalculator = [[BCToolHydropathyCalculator alloc] initWithSequence: list];
	[hydropathyCalculator setSlidingWindowSize: 1];	// default value

	return [hydropathyCalculator autorelease];
}

- (BCHydropathyType)hydropathyType
{
	return hydropathyType;
}

- (void)setHydropathyType:(BCHydropathyType)type
{
	hydropathyType = type;
}

- (int)slidingWindowSize
{
	return slidingWindowSize;
}

- (void)setSlidingWindowSize:(int)newSize
{
	slidingWindowSize = newSize;
}

-(NSArray *)calculateHydropathy
{
	return [self calculateHydropathyForRange: NSMakeRange(0, [[self sequence] length])];
}


-(NSArray *)calculateHydropathyForRange: (NSRange)aRange
{
	unsigned int	len, start, i, j;
	BCAminoAcid		*aa;
	NSMutableArray	*tempArray = [NSMutableArray array];
//	NSArray			*sequenceArray = [[self sequence] symbolArray];
//	CFIndex			i, j;
	float			sum;
	
	len = [[self sequence] length];
	
	if ( len > 0 )
	{
		start = aRange.location + 1;
		
		for ( i = 0; i < (len - [self slidingWindowSize]); i++ )
		{
			sum = 0.0;
			
			for ( j = 0; j < [self slidingWindowSize]; j++ )
			{
	//			aa = (BCAminoAcid *)CFArrayGetValueAtIndex((CFArrayRef) sequenceArray, i+j );				// use NSData instead ?
				aa = (BCAminoAcid *) [[self sequence] symbolAtIndex: (i+j)];
				sum += (hydropathyType == BCKyteDoolittle ? [aa kyteDoolittleValue] : [aa hoppWoodsValue]);
			}
			
			[tempArray addObject: NSStringFromPoint( NSMakePoint( (float) (start + i), (float) (sum / [self slidingWindowSize] ) ) ) ];
		}
	}
	
	return	[NSArray arrayWithArray: tempArray];
}


@end
