//
//  BCToolTranslatorDNA.m
//  BioCocoa
//
//  Created by John Timmer on 8/29/04.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCToolTranslatorDNA.h"

#import "BCNucleotideDNA.h"
#import "BCAminoAcid.h"
#import "BCSequenceCodon.h"
#import "BCCodonDNA.h"
#import "BCGeneticCode.h"

@implementation BCToolTranslatorDNA


+ (BCToolTranslatorDNA *) dnaTranslatorToolWithSequence: (BCSequence *) list
{
     BCToolTranslatorDNA *translatorTool = [[BCToolTranslatorDNA alloc] initWithSequence: list];
     
	 return [translatorTool autorelease];
}

/*
+ (BCSequenceCodon *) translateSequence: (BCSequence*) entry usingGeneticCode: (BCGeneticCodeName) codeName
{
    return nil;
}
*/

- (NSArray *)translateDNASequence
{
	return [BCToolTranslatorDNA translateDNASequence: [self sequence] usingGeneticCode: [self codeName]];
}


- (BCGeneticCodeName) codeName
{
	return codeName;
}


- (void)setCodeName: (BCGeneticCodeName)aName
{
	codeName = aName;
}

+ (id) translateSequence: (BCSequence*) entry usingGeneticCode: (BCGeneticCodeName) codeName {
    return nil;
}

+ (NSArray *) translateDNASequence: (BCSequence *) entry usingGeneticCode: (BCGeneticCodeName) codeName {
    NSArray *theCode = [BCGeneticCode universalGeneticCodeDNA];
    if ( theCode == nil )
        return nil;
    
    int codonCount = [theCode count];
    int loopCounter, innerCounter;
    NSArray *theSequenceArray = [entry symbolArray];
    NSMutableArray *returnArray = [NSMutableArray array];
    NSArray *tempCodon;
    BCCodonDNA *aCodon;
    BOOL oneMatch;
    for ( loopCounter = 0 ; loopCounter + 2 < [entry length] ; loopCounter = loopCounter + 3 ) {
        tempCodon = [theSequenceArray subarrayWithRange: NSMakeRange( loopCounter, 3 ) ];
        oneMatch = NO;
        
        for ( innerCounter = 0 ; innerCounter < codonCount ; innerCounter++ ) {
            aCodon = [theCode objectAtIndex: innerCounter];
            if ( [aCodon matchesTriplet: tempCodon] ) {
                [returnArray addObject: aCodon];
                oneMatch = YES;
                break;
            }
        }
        
        if ( !oneMatch )
            [returnArray addObject: [BCCodonDNA unmatched]];
        
    }
    
    return [[returnArray copy] autorelease];
}
@end
