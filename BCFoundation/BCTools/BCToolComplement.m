//
//  BCToolComplement.m
//  BioCocoa
//
//  Created by Koen van der Drift on 11/19/04.
//  Copyright (c) 2003-2009 The BioCocoa Project.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//  1. Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the distribution.
//  3. The name of the author may not be used to endorse or promote products
//  derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
//  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import "BCToolComplement.h"
#import "BCSequence.h"
#import "BCSymbol.h"
#import "BCNucleotide.h"

#import "BCFoundationDefines.h"

#import "BCInternal.h"

@implementation BCToolComplement

-(id) initWithSequence:(BCSequence *)list
{
    if ( (self = [super initWithSequence:list]) )
    {
		[self setReverse: NO];
	}
	
	return self;
}


+ (BCToolComplement *) complementToolWithSequence: (BCSequence *) list
{
     BCToolComplement *complementTool = [[BCToolComplement alloc] initWithSequence: list];
     
	 return [complementTool autorelease];
}


- (void)setReverse: (BOOL)value
{
	reverse = value;
}

- (BCSequence *) sequenceComplement
{
	BCNucleotide		*symbol;
	NSArray			*symbolArray;
	NSMutableArray	*theComplement;
	DECLARE_INDEX(loopCounter);
	int				theLimit, newLocation;
	BCSymbol		*symbolComplement;
	
	// if it's a protein, we return the same sequence.
	
	// why a negative test here? It could also be BCSequenceTypeCodon or BCSequenceTypeOther.
	// maybe we should test for BCSequenceTypeProtein ?
	
	if ( [sequence sequenceType] != BCSequenceTypeDNA && [sequence sequenceType] != BCSequenceTypeRNA )
            return [[sequence copy] autorelease];
        
        
	symbolArray = [[self sequence] symbolArray];
	theComplement = [NSMutableArray array];
	theLimit = [symbolArray count];
    
    for ( loopCounter = 0 ; loopCounter < theLimit ; loopCounter++ )
	{
	symbol = (id)ARRAY_GET_VALUE_AT_INDEX(symbolArray, loopCounter);
        symbolComplement = [symbol complement];
		
		if ( symbolComplement != nil )
		{
			newLocation = ( reverse == NO ? loopCounter : 0 );

			ARRAY_INSERT_VALUE_AT_INDEX(theComplement, newLocation, symbolComplement);
		}
	 }
	 
     return [BCSequence sequenceWithSymbolArray: theComplement symbolSet: [[self sequence] symbolSet]];
}

@end
